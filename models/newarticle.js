var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var newArticle = new Schema({

    articleType: {
        type: Number,
        required: true
    },
    detailInfoUrl: {
        type: String,
        required: true
    },
    thumbUrl: {
        type: String,
    },
    title: {
        type: String,
    },
    startTime: {
        type: Date,
        default: Date.now
    },
    registeredDate: {
        type: Date,
        default: Date.now
    },
    sns_type: {

    }
});

module.exports = mongoose.model('newArticle', newArticle);