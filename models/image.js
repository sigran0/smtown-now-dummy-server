let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let imageSchema = new Schema({

    title: String,
    imageUrl: String,
    width: Number,
    height: Number
});

module.exports = mongoose.model('image', imageSchema);