var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*
*
*   TYPE
*   0 : article
*   1 : theatre
* */
var articleSchema = new Schema({

    title: {
        type:String,
        required: true
    },
    contents: {
        type: String,
        required: true
    },
    image: {
        title: String,
        imageUrl: String,
        width: Number,
        height: Number
    },
    linkUrl: {
        type: String
    },
    like: {
        type: Number,
        default: 0
    },
    type: {
        type: Number,
        default: 0
    },
    sns_type: {
        type: Number,
        default: -1
    },
    contents_url: {
        type: String,
        default: null
    },
    date: {
        type: Number,
    }
});

module.exports = mongoose.model('article', articleSchema);