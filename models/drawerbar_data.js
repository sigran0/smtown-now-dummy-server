let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let drawerbar_menu = new Schema({

    Services: [
        {
            type: Number,
            title: String,
            package: String,
            thumbnail_image: String
        }
    ]
});

module.exports = mongoose.model('drawerbar_menu', drawerbar_menu);