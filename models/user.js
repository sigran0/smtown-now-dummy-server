var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({

    userId: {
        type: String,
        unique: true,
        require: true
    },
    userEmail: String,
    password: {
        type: String,
        required: true,
        select: true
    },
    image: Schema.Types.ObjectId
});

userSchema.pre('save', function(next) {
    let user = this;

    if(this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, (err, salt) => {
            if(err)
                return next(err);

            bcrypt.hash(user.password, salt, null, (err, hash) => {
                if(err)
                    return next(err);
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

userSchema.methods.comparePassword = function(pw, cb) {

    bcrypt.compare(pw, this.password, function(err, isMatch) {
        if(err)
            return cb(err);

        cb(null, isMatch);
    });
};

module.exports = mongoose.model('user', userSchema);