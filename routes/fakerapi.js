
let express = require('express');
let router = express.Router();
let path = require('path');
let fs = require('fs');

let faker = require('faker');
let jsf = require('json-schema-faker');

faker.locale = 'ko';

jsf.extend('faker', function(){
    var faker = require('faker');

    faker.locale = "ko"; // or any other language
    faker.custom = {
        statement: function(length) {
            return faker.name.firstName() + " has " + faker.finance.amount() + " on " + faker.finance.account(length) + ".";
        }
    };
    return faker;
});

let mainHomeSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: "success",
        CardNews: {
            type: 'array',
            minItems: 6,
            maxItems: 6,
            items: {
                type: 'object',
                properties: {
                    thumbUrl: "http://via.placeholder.com/350x500",
                    title : {
                        type: 'string',
                        faker: 'name.title'
                    },
                    detailInfoUrl: {
                        type: 'string',
                        pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                    }
                },
                required: ['thumbUrl', 'title', 'detailInfoUrl']
            }
        },
        ListNews: {
            type: 'array',
            minItems: 5,
            maxItems: 5,
            items: {
                type: 'object',
                properties: {
                    title: {
                        type: 'string',
                        faker: 'name.title'
                    },
                    detailInfoUrl: {
                        type: 'string',
                        pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                    }
                },
                required: ['title', 'detailInfoUrl']
            }
        },
        Banner: {
            type: 'array',
            minItems: 5,
            maxItems: 5,
            items: {
                type: 'object',
                properties: {
                    thumbUrl: 'http://via.placeholder.com/1242x260',
                    detailInfoUrl: {
                        type: 'string',
                        pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                    }
                },
                required: ['thumbUrl', 'detailInfoUrl']
            }
        },
        Theatre: {
            type: 'array',
            minItems: 10,
            maxItems: 10,
            items: {
                type: 'object',
                properties: {
                    thumbUrl: "http://via.placeholder.com/350x500",
                    startTime: {
                        $ref: '#/definitions/dateNumber'
                    },
                    title : {
                        type: 'string',
                        faker: 'name.title'
                    },
                    detailInfoUrl: {
                        type: 'string',
                        pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                    }
                },
                required: ['thumbUrl', 'startTime', 'title', 'detailInfoUrl']
            }
        },
        Artwork: {
            type: 'array',
            minItems: 10,
            maxItems: 10,
            items: {
                type: 'object',
                properties: {
                    thumbUrl: "http://via.placeholder.com/500x500",
                    title : {
                        type: 'string',
                        faker: 'name.title'
                    },
                    detailInfoUrl: {
                        type: 'string',
                        pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                    },
                    registeredDate : {
                        $ref: '#/definitions/dateNumber'
                    },
                    width: {
                        $ref: '#/definitions/size'
                    },
                    height: {
                        $ref: '#/definitions/size'
                    }
                },
                required: ['thumbUrl', 'title', 'detailInfoUrl', 'registeredDate', 'width', 'height']
            }
        },
        Youtube: {
            type: 'array',
            minItems: 6,
            maxItems: 6,
            items: {
                type: 'object',
                properties: {
                    thumbUrl: "http://via.placeholder.com/1186x665",
                    title : {
                        type: 'string',
                        faker: 'name.title'
                    },
                    detailInfoUrl: {
                        type: 'string',
                        pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                    },
                    youtubeId: {
                        type: 'string',
                        pattern: 'lX8on7n9Ks8|WyiIGEHQP8o|QslJYDX3o8s|ePpPVE-GGJw|c7rCyll5AeY|8A2t_tAjMz8|VQtonf1fv_s'
                    },
                    registeredDate : {
                        $ref: '#/definitions/dateNumber'
                    }
                },
                required: ['thumbUrl', 'title', 'detailInfoUrl', 'youtubeId','registeredDate']
            }
        },
        OfficialSns: {
            type: 'array',
            minItems: 15,
            maxItems: 15,
            items: {
                type: 'object',
                properties: {
                    type: {
                        $ref: '#/definitions/rangeInt'
                    },
                    thumbUrl: "http://via.placeholder.com/236x236",
                    snsIconUrl: "http://via.placeholder.com/64x64",
                    title : {
                        type: 'string',
                        faker: 'name.title'
                    },
                    detailInfoUrl: {
                        type: 'string',
                        pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                    }
                },
                required: ['type', 'thumbUrl', 'snsIconUrl', 'title', 'detailInfoUrl']
            }
        }
    },
    definitions: {
        rangeInt: {
            type: 'integer',
            minimum: 0,
            maximum: 4
        },
        dateNumber: {
            type: 'integer',
            minimum: 1000000000,
            maximum: 1509590121
        },
        size: {
            type: 'integer',
            minimum: 200,
            maximum: 1000
        }
    },
    required: ['code', 'message', 'CardNews', 'ListNews', 'Banner', 'Theatre', 'Artwork', 'Youtube', 'OfficialSns']
};

let mainHomeTheatreSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: "success",
        Theatre: {
            type: 'array',
            minItems: 0,
            maxItems: 10,
            items: {
                type: 'object',
                properties: {
                    startTime: {
                        $ref: '#/definitions/dateNumber'
                    },
                    thumbUrl: "http://via.placeholder.com/350x500",
                    title: {
                        type: 'string',
                        faker: 'name.title'
                    },
                    detailInfoUrl: {
                        type: 'string',
                        pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                    }
                },
                required: ['startTime', 'thumbUrl', 'title', 'detailInfoUrl']
            }
        }
    },
    definitions: {
        dateNumber: {
            type: 'integer',
            minimum: 1000000000,
            maximum: 1509590121
        }
    },
    required: ['code', 'message', 'Theatre']
};

let mainNewsSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: "success",
        NewsInfo: {
            type: 'object',
            properties: {
                page: 1,
                perpage: 10,
                pages: 3,
                total: 150,
                News: {
                    type: 'array',
                    minItems: 10,
                    maxItems: 10,
                    items: {
                        type: 'object',
                        properties: {
                            thumbUrl: "http://via.placeholder.com/350x500",
                            id: {
                                $ref: '#/definitions/articleNumber'
                            },
                            width: {
                                $ref: '#/definitions/size'
                            },
                            height: {
                                $ref: '#/definitions/size'
                            },
                            title: {
                                type: 'string',
                                faker: 'name.title'
                            },
                            registerDate: {
                                $ref: '#/definitions/dateNumber'
                            },
                            likeCount: {
                                $ref: "#/definitions/likeCount"
                            },
                            viewCount: {
                                $ref: '#/definitions/viewCount'
                            },
                            sourceType: {
                                $ref: '#/definitions/sourceType'
                            },
                            sourceIconUrl: "http://via.placeholder.com/64x64",
                            sourceName: {
                                type: 'string',
                                faker: 'name.findName'
                            },
                            youtubeId: {
                                type: 'string',
                                pattern: 'lX8on7n9Ks8|WyiIGEHQP8o|QslJYDX3o8s|ePpPVE-GGJw|c7rCyll5AeY|8A2t_tAjMz8|VQtonf1fv_s'
                            },
                            detailInfoUrl: {
                                type: 'string',
                                pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                            }
                        }, required: ['thumbUrl', 'id', 'width', 'height', 'title', 'registerDate', 'likeCount', 'viewCount', 'sourceType', 'sourceIconUrl', 'sourceName', 'youtubeId', 'detailInfoUrl']
                    }
                }
            }, required: ['page', 'perpage', 'pages', 'total', 'News']
        }
    },
    definitions: {
        dateNumber: {
            type: 'integer',
            minimum: 1000000000,
            maximum: 1509590121
        },
        size: {
            type: 'integer',
            minimum: 256,
            maximum: 1024
        },
        articleNumber: {
            type: 'integer',
            minimum: 0,
            maximum: 1000
        },
        sourceType: {
            type: 'integer',
            minimum: 0,
            maximum: 2
        },
        likeCount: {
            type: 'integer',
            minimum: 0,
            maximum: 100000
        },
        viewCount: {
            type: 'integer',
            minimum: 0,
            maximum: 1000000000
        }
    },
    required: ['code', 'message', 'NewsInfo']
};

let mainPhotoSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: "success",
        PhotoInfo: {
            type: 'object',
            properties: {
                page: 1,
                perpage: 10,
                pages: 3,
                total: 150,
                Photo: {
                    type: 'array',
                    minItems: 10,
                    maxItems: 10,
                    items: {
                        type: 'object',
                        properties: {
                            id: {
                                $ref: '#/definitions/articleNumber'
                            },
                            thumbUrl: "http://via.placeholder.com/350x500",
                            width: {
                                $ref: '#/definitions/size'
                            },
                            height: {
                                $ref: '#/definitions/size'
                            },
                            title: {
                                type: 'string',
                                faker: 'name.title'
                            },
                            registerDate: {
                                $ref: '#/definitions/dateNumber'
                            },
                            likeCount: {
                                $ref: '#/definitions/likeCount'
                            },
                            sourceType: {
                                $ref: '#/definitions/sourceType'
                            },
                            detailInfoUrl: {
                                type: 'string',
                                pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                            }
                        }, required: ['id', 'thumbUrl', 'width', 'height', 'title', 'registerDate', 'likeCount', 'sourceType', 'detailInfoUrl']
                    }
                }
            }, required: ['page', 'perpage', 'pages', 'total', 'Photo']
        }
    },
    definitions: {
        dateNumber: {
            type: 'integer',
            minimum: 1000000000,
            maximum: 1509590121
        },
        size: {
            type: 'integer',
            minimum: 256,
            maximum: 1024
        },
        articleNumber: {
            type: 'integer',
            minimum: 0,
            maximum: 1000
        },
        sourceType: {
            type: 'integer',
            minimum: 3,
            maximum: 7
        },
        likeCount: {
            type: 'integer',
            minimum: 0,
            maximum: 100000
        }
    }, required: ['code', 'message', 'PhotoInfo']
};

let mainVideoSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: "success",
        VideoInfo: {
            type: 'object',
            properties: {
                page: 1,
                perpage: 10,
                pages: 5,
                total: 150,
                Video: {
                    type: 'array',
                    minItems: 10,
                    maxItems: 10,
                    items: {
                        type: 'object',
                        properties: {
                            id: {
                                $ref: '#/definitions/articleNumber'
                            },
                            thumbUrl: "http://via.placeholder.com/350x500",
                            width: {
                                $ref: '#/definitions/size'
                            },
                            height: {
                                $ref: '#/definitions/size'
                            },
                            title: {
                                type: 'string',
                                faker: 'name.title'
                            },
                            description: {
                                type: 'string',
                                faker: 'lorem.paragraph'
                            },
                            runningTime: {
                                $ref: '#/definitions/runningTime'
                            },
                            registerDate: {
                                $ref: '#/definitions/dateNumber'
                            },
                            viewCount: {
                                $ref: '#/definitions/viewCount'
                            },
                            sourceType: {
                                $ref: '#/definitions/sourceType'
                            },
                            sourceIconUrl: 'http://lorempixel.com/64/64',
                            sourceName: {
                                type: 'string',
                                faker: 'name.findName'
                            },
                            youtubeId: {
                                type: 'string',
                                pattern: 'lX8on7n9Ks8|WyiIGEHQP8o|QslJYDX3o8s|ePpPVE-GGJw|c7rCyll5AeY|8A2t_tAjMz8|VQtonf1fv_s'
                            },
                            detailInfoUrl: {
                                type: 'string',
                                pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                            }
                        }, required: ['id', 'thumbUrl', 'width', 'height', 'title', 'description', 'runningTime', 'registerDate', 'viewCount', 'sourceType', 'sourceIconUrl', 'sourceName', 'youtubeId', 'detailInfoUrl']
                    }
                }
            }, required: ['page', 'perpage', 'pages', 'total', 'Video']
        }
    },
    definitions: {
        dateNumber: {
            type: 'integer',
            minimum: 1000000000,
            maximum: 1509590121
        },
        size: {
            type: 'integer',
            minimum: 256,
            maximum: 1024
        },
        articleNumber: {
            type: 'integer',
            minimum: 0,
            maximum: 1000
        },
        sourceType: {
            type: 'integer',
            minimum: 3,
            maximum: 7
        },
        runningTime: {
            type: 'integer',
            minimum: 30,
            maximum: 600
        },
        viewCount: {
            type: 'integer',
            minimum: 0,
            maximum: 1000000000
        }
    }, required: ['code', 'message', 'VideoInfo']
};

let mainSnsSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: "success",
        SnsInfo: {
            type: 'object',
            properties: {
                page: 1,
                perpage: 10,
                pages: 5,
                total: 150,
                Sns: {
                    type: 'array',
                    minItems: 10,
                    maxItems: 10,
                    items: {
                        type: 'object',
                        properties: {
                            id: {
                                $ref: '#/definitions/articleNumber'
                            },
                            thumbUrl: "http://via.placeholder.com/350x500",
                            width: {
                                $ref: '#/definitions/size'
                            },
                            height: {
                                $ref: '#/definitions/size'
                            },
                            title: {
                                type: 'string',
                                faker: 'name.title'
                            },
                            registerDate: {
                                $ref: '#/definitions/dateNumber'
                            },
                            likeCount: {
                                $ref: '#/definitions/likeCount'
                            },
                            sourceType: {
                                $ref: '#/definitions/sourceType'
                            },
                            sourceIconUrl: 'http://lorempixel.com/64/64',
                            sourceName: {
                                type: 'string',
                                faker: 'name.findName'
                            },
                            detailInfoUrl: {
                                type: 'string',
                                pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                            }
                        }, required: ['id', 'thumbUrl', 'width', 'height', 'title', 'registerDate', 'likeCount', 'sourceType', 'sourceIconUrl', 'sourceName', 'detailInfoUrl']
                    }
                }
            }, required: ['page', 'perpage', 'pages', 'total', 'Sns']
        }
    },
    definitions: {
        dateNumber: {
            type: 'integer',
            minimum: 1000000000,
            maximum: 1509590121
        },
        size: {
            type: 'integer',
            minimum: 256,
            maximum: 1024
        },
        articleNumber: {
            type: 'integer',
            minimum: 0,
            maximum: 1000
        },
        sourceType: {
            type: 'integer',
            minimum: 3,
            maximum: 7
        },
        likeCount: {
            type: 'integer',
            minimum: 0,
            maximum: 100000
        }
    }, required: ['code', 'message', 'SnsInfo']
};

let favoriteArtist = {
    type: 'object',
    properties: {
        code: 20000,
        message: "success",
        FavoriteArtist: {
            type: 'array',
            minItems: 20,
            maxItems: 20,
            items: {
                type: 'object',
                properties: {
                    id: {
                        type: 'number',
                        unique: true,
                        minimum: 1
                    },
                    name: {
                        type: 'string',
                        faker: 'name.findName'
                    },
                    selected: {
                        type: 'boolean',
                        faker: 'random.boolean'
                    }
                }, required: ['id', 'name', 'selected']
            }
        }
    }, required: ['code', 'message', 'FavoriteArtist']
};

let favoriteAdd = {
    type: 'object',
    properties: {
        code: 20000,
        message: "success",
        SnsInfo: {
            type: 'array',
            minItems: 1,
            maxItems: 1,
            items: {
                type: 'object',
                properties: {
                    id: {
                        type: 'number',
                        unique: true,
                        minimum: 1
                    },
                    name: {
                        type: 'string',
                        faker: 'name.findName'
                    },
                    selected: {
                        type: 'boolean',
                        faker: 'random.boolean'
                    }
                }, required: ['id', 'name', 'selected']
            }
        }
    }, required: ['code', 'message', 'SnsInfo']
};

let searchContentSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: "success",
        SearchInfo: {
            type: 'object',
            properties: {
                page: 1,
                perpage: 10,
                pages: 15,
                total: 150,
                SearchResult: {
                    type: 'array',
                    minItems: 10,
                    maxItems: 10,
                    items: {
                        type: 'object',
                        properties: {
                            type: {
                                $ref: '#/definitions/type'
                            },
                            thumbUrl: "http://via.placeholder.com/320x480",
                            title: {
                                type: 'string',
                                faker: 'name.title'
                            },
                            width: {
                                $ref: '#/definitions/randomSize'
                            },
                            height: {
                                $ref: '#/definitions/randomSize'
                            },
                            description: {
                                type: 'string',
                                faker: 'lorem.paragraph'
                            },
                            likeCount: {
                                $ref: '#/definitions/articleNumber'
                            },
                            registerDate: {
                                $ref: '#/definitions/dateNumber'
                            },
                            sourceType: {
                                $ref: '#/definitions/sourceType'
                            },
                            sourceIconUrl: "http://lorempixel.com/64/64/",
                            sourceName: {
                                type: 'string',
                                faker: 'name.findName'
                            },
                            detailInfoUrl: {
                                type: 'string',
                                pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                            }
                        }, required: ['type', 'thumbUrl', 'title', 'width', 'height', 'description', 'likeCount', 'registerDate', 'sourceType', 'sourceIconUrl', 'sourceName', 'detailInfoUrl']
                    }
                }
            }, required: ['page', 'perpage', 'pages', 'total', 'SearchResult']
        }
    },
    definitions: {
        type: {
            type: 'integer',
            minimum: 0,
            maximum: 1
        },
        dateNumber: {
            type: 'integer',
            minimum: 1000000000,
            maximum: 1509590121
        },
        articleNumber: {
            type: 'integer',
            minimum: 0,
            maximum: 1000
        },
        randomSize: {
            type: 'integer',
            minimum: 256,
            maximum: 1024
        },
        sourceType: {
            type: 'integer',
            minimum: 3,
            maximum: 7
        }
    },
    required: ['code', 'message', 'SearchInfo']
};

let searchKeywordSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: 'success',
        SearchKeyword: {
            type: 'array',
            minItems: 0,
            maxItems: 10,
            items: {
                type: 'object',
                properties: {
                    word: {
                        type: 'string',
                        faker: 'lorem.word'
                    }
                }, required: ['word']
            }
        }
    }, required: ['code', 'message', 'SearchKeyword']
};

let serviceSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: 'success',
        Service: {
            type: 'array',
            minItems: 10,
            maxItems: 10,
            items: {
                type: 'object',
                properties: {
                    id: {
                        type: 'number',
                        unique: true,
                        minimum: 1
                    },
                    iconUrl: 'http://via.placeholder.com/128x128',
                    name: {
                        type: 'string',
                        faker: 'system.fileName'
                    },
                    launchUrl: {
                        type: 'string',
                        pattern: 'https^:^/^/now\\.smtown\\.com^/^#^/Show^/[0-9]{4,4}'
                    },
                    urlScheme: 'com.smtown.fanbook://',
                    storeId: '1149315076'
                }, required: ['id', 'iconUrl', 'name', 'launchUrl', 'urlScheme', 'storeId']
            }
        }
    }, required: ['code', 'message', 'Service']
};

let singinSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: 'success',
        User: {
            type: 'object',
            properties: {
                userId: {
                    type: 'number',
                    $ref: '#/definitions/userId'
                },
                name: {
                    type: 'string',
                    faker: 'name.findName'
                },
                accountType: {
                    type: 'number',
                    $ref: '#/definitions/paddingZero'
                },
                approvalAdvertisement: {
                    type: 'boolean',
                    faker: 'random.boolean'
                },
                approvalFavoriteArtist: {
                    type: 'boolean',
                    faker: 'random.boolean'
                },
                pushOn: {
                    type: 'boolean',
                    faker: 'random.boolean'
                }
            }, required: ['userId', 'name', 'accountType', 'approvalAdvertisement', 'approvalFavoriteArtist', 'pushOn']
        }
    },
    definitions: {
        paddingZero:{
            type: 'integer',
            minimum: 1,
            maximum: 3
        },
        userId: {
            type: 'integer',
            minimum: 0,
            maximum: 1000
        }
    },
    required: ['code', 'message', 'User']
};

let smtownSinginSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: 'success',
        User: {
            type: 'object',
            properties: {
                userId: {
                    type: 'number',
                    $ref: '#/definitions/userId'
                },
                name: {
                    type: 'string',
                    faker: 'name.findName'
                },
                accountType: 1,
                approvalAdvertisement: {
                    type: 'boolean',
                    faker: 'random.boolean'
                },
                pushOn: {
                    type: 'boolean',
                    faker: 'random.boolean'
                }
            }, required: ['userId', 'name', 'accountType', 'approvalAdvertisement', 'pushOn']
        }
    },
    definitions: {
        paddingZero:{
            type: 'integer',
            minimum: 1,
            maximum: 1
        },
        userId: {
            type: 'integer',
            minimum: 0,
            maximum: 1000
        }
    },
    required: ['code', 'message', 'User']
};

let facebookSinginSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: 'success',
        User: {
            type: 'object',
            properties: {
                userId: {
                    type: 'number',
                    $ref: '#/definitions/userId'
                },
                name: {
                    type: 'string',
                    faker: 'name.findName'
                },
                accountType: 2,
                approvalAdvertisement: {
                    type: 'boolean',
                    faker: 'random.boolean'
                },
                pushOn: {
                    type: 'boolean',
                    faker: 'random.boolean'
                }
            }, required: ['userId', 'name', 'accountType', 'approvalAdvertisement', 'pushOn']
        }
    },
    definitions: {
        paddingZero:{
            type: 'integer',
            minimum: 0,
            maximum: 2
        },
        userId: {
            type: 'integer',
            minimum: 0,
            maximum: 1000
        }
    },
    required: ['code', 'message', 'User']
};

let twitterSinginSchema = {
    type: 'object',
    properties: {
        code: 20000,
        message: 'success',
        User: {
            type: 'object',
            properties: {
                userId: {
                    type: 'number',
                    $ref: '#/definitions/userId'
                },
                name: {
                    type: 'string',
                    faker: 'name.findName'
                },
                accountType: 3,
                approvalAdvertisement: {
                    type: 'boolean',
                    faker: 'random.boolean'
                },
                pushOn: {
                    type: 'boolean',
                    faker: 'random.boolean'
                }
            }, required: ['userId', 'name', 'accountType', 'approvalAdvertisement', 'pushOn']
        }
    },
    definitions: {
        paddingZero:{
            type: 'integer',
            minimum: 0,
            maximum: 2
        },
        userId: {
            type: 'integer',
            minimum: 0,
            maximum: 1000
        }
    },
    required: ['code', 'message', 'User']
};

let testSchema = {
    type: 'object',
    properties: {
        width: {
            $ref: "#/definitions/width"
        },
        height: {
            $ref: "#/definitions/height"
        },
        good: {
            $ref: "#/definitions/width"
        },
        fuck: {
            $ref: "#/definitions/height"
        }
    },
    definitions: {
        width: {
            type: 'integer',
            minimum: 150,
            maximum: 350
        },
        height: {
            type: 'integer',
            minimum: 150,
            maximum: 350
        }
    },
    required:['width', 'height', 'good', 'fuck']
};

let testSchema2 = {
    type: 'object',
    properties: {
        date: {
            type: 'long',
            faker: 'date.recent'
        }
    },
    required: ['date']
};

router.get('/good', (req, res) => {

    let data = jsf(testSchema2);

    res.json(data);
});

router.get('/test ', (req, res) => {

    let data = jsf(testSchema);

    res.json(data);
});

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

router.get('/main/home', (req, res) => {

    let data = jsf(mainHomeSchema);

    res.json(data);
});

router.post('/main/home', (req, res) => {

    let data = jsf(mainHomeSchema);

    res.json(data);
});

router.get('/main/home/Theatre', (req, res) => {

    let data = jsf(mainHomeTheatreSchema);

    res.json(data);
});

router.post('/main/home/Theatre', (req, res) => {

    let data = jsf(mainHomeTheatreSchema);

    res.json(data);
});

router.get('/main/News', (req, res) => {

    let data = jsf(mainNewsSchema);

    res.json(data);
});

router.post('/main/News', (req, res) => {

    let data = jsf(mainNewsSchema);

    res.json(data);
});

router.get('/main/Photo', (req, res) => {

    let data = jsf(mainPhotoSchema);

    res.json(data);
});

router.post('/main/Photo', (req, res) => {

    let data = jsf(mainPhotoSchema);

    res.json(data);
});

router.get('/main/Video', (req, res) => {

    let data = jsf(mainVideoSchema);

    res.json(data);
});

router.post('/main/Video', (req, res) => {

    let data = jsf(mainVideoSchema);

    res.json(data);
});

router.get('/main/sns', (req, res) => {

    let data = jsf(mainSnsSchema);

    res.json(data);
});

router.post('/main/sns', (req, res) => {

    let data = jsf(mainSnsSchema);

    res.json(data);
});

router.get('/favorite/artist', (req, res) => {

    let data = jsf(favoriteArtist);

    res.json(data);
});

router.post('/favorite/artist', (req, res) => {

    let data = jsf(favoriteArtist);

    res.json(data);
});

router.get('/favorite/add', (req, res) => {

    let data = jsf(favoriteArtist);

    res.json(data);
});

router.post('/favorite/add', (req, res) => {

    let data = jsf(favoriteArtist);

    res.json(data);
});

router.get('/favorite/delete', (req, res) => {

    res.json({
        code: 20000,
        message: 'success'
    });
});

router.post('/favorite/delete', (req, res) => {

    res.json({
        code: 20000,
        message: 'success'
    });
});

router.get('/search/content', (req, res) => {

    let data = jsf(searchContentSchema);

    res.json(data);
});

router.post('/search/content', (req, res) => {

    let keyword = req.body.keyword;
    let success = req.body.success | 1;

    if(success === 1){
        let data = jsf(searchContentSchema);

        res.json(data);
    } else {
        let resultStr = keyword + '를 만족하는 결과를 찾지 못했습니다';

        res.json({
            code: 20003,
            message: resultStr
        })
    }
});

router.get('/search/keyword', (req, res) => {

    let data = jsf(searchKeywordSchema);

    res.json(data);
});

router.post('/search/keyword', (req, res) => {

    let keyword = req.body.word;
    let success = req.body.success | 1;

    if(success === 1){
        let data = jsf(searchKeywordSchema);

        res.json(data);
    } else {
        let resultStr = keyword + '를 만족하는 결과를 찾지 못했습니다';

        res.json({
            code: 20003,
            message: resultStr
        })
    }
});

router.get('/service', (req, res) => {

    let data = jsf(serviceSchema);

    res.json(data);
});

router.post('/service', (req, res) => {

    let system = req.body.system;

    if(system === 0){    //  ios
        let data = jsf(serviceSchema);

        res.json(data);
    } else {            // android

        let data = jsf(serviceSchema);

        res.json(data);
    }
});

router.get('/comments/report', (req, res) => {

    let success = 1;

    if(success === 1) {
        res.json({
            code: 20000,
            message: '신고가 완료 되었습니다 담당자 확인 후 삭제 또는 해제 예정입니다. 감사합니다.'
        });
    } else {
        res.json({
            code: 2002,
            message: '일일 신고 가능한 횟수를 초과 하였습니다.'
        })
    }
});

router.post('/comments/report', (req, res) => {

    let success = req.body.success | 1;

    if(success === 1) {
        res.json({
            code: 20000,
            message: '신고가 완료 되었습니다 담당자 확인 후 삭제 또는 해제 예정입니다. 감사합니다.'
        });
    } else {
        res.json({
            code: 2002,
            message: '일일 신고 가능한 횟수를 초과 하였습니다.'
        })
    }
});

router.get('/validate/version', (req, res) => {

    let success = 1;

    let buildVersion = req.body.buildVersion;

    if(buildVersion === undefined)
        buildVersion = 0;

    if(buildVersion != 0) {
        res.json({
            code: 20000,
            message: 'success',
            WebViewUrls: {
                notice: 'https://m.naver.com',
                term: 'https://www.google.co.kr/',
                privacy: 'https://www.daum.net/'
            }
        });
    } else {
        res.json({
            code: 20001,
            message: '최신 버전으로 업데이트 바랍니다.'
        });
    }
});

router.post('/validate/version', (req, res) => {

    let success = 1;

    let buildVersion = req.body.buildVersion;

    if(buildVersion === undefined)
        buildVersion = 1;

    if(buildVersion != 0) {
        res.json({
            code: 20000,
            message: 'success',
            WebViewUrls: {
                notice: 'https://m.naver.com',
                term: 'https://www.google.co.kr/',
                privacy: 'https://www.daum.net/'
            }
        });
    } else {
        res.json({
            code: 20001,
            message: '최신 버전으로 업데이트 바랍니다.'
        });
    }
});

router.get('/signin', (req, res) => {

    let success = 1;

    if(success === 1) {

        let data = jsf(singinSchema);
        res.json(data);
    } else {
        res.json({
            code: 20007,
            message: 'fail to sign in'
        });
    }
});

router.post('/signin', (req, res) => {

    let success = req.body.success | 1;
    let type = req.body.accountType | 99;

    if(req.body.accountType !== undefined)
        type = req.body.accountType;
    else
        type = -1;

    if(success == 1) {
        let data = '';
        if(type == 1) {
            data = jsf(smtownSinginSchema);
        } else if(type == 2) {
            data = jsf(facebookSinginSchema);
        } else if(type == 3) {
            data = jsf(twitterSinginSchema)
        } else {
            data = jsf(singinSchema);
        }
        res.json(data);
    } else {
        res.json({
            code: 20007,
            message: 'fail to sign in'
        });
    }
});

router.get('/signout', (req, res) => {

    let success = 1;

    if(success === 1) {

        res.json({
            code: 20000,
            message: 'success'
        });
    } else {
        res.json({
            code: 20005,
            message: 'fail to sign out'
        });
    }
});

router.post('/signout', (req, res) => {

    let success = req.body.success | 1;

    if(success === 1) {

        res.json({
            code: 20000,
            message: 'success'
        });
    } else {
        res.json({
            code: 20005,
            message: 'fail to sign out'
        });
    }
});

router.get('/approval/push/advertisement', (req, res) => {

    res.json({
        code: 20000,
        "message": "회원님은 2017년 07월 10일 SMTOWNNOW 마케팅 정보 수신에 ‘동의‘ 하셨습니다."
    });
});

router.post('/approval/push/advertisement', (req, res) => {

    res.json({
        code: 20000,
        "message": "회원님은 2017년 07월 10일 SMTOWNNOW 마케팅 정보 수신에 ‘동의‘ 하셨습니다."
    });
});

router.get('/approval/push/favorite', (req, res) => {

    res.json({
        code: 20000,
        "message": "success"
    });
});

router.post('/approval/push/favorite', (req, res) => {

    res.json({
        code: 20000,
        "message": "success"
    });
});

router.post('/postGetDrawerBarData', (req, res) => {

    __parentdir = path.dirname(module.parent.filename);

    //  first, read drawerbar data in the data/drawerbar_data.json
    fs.readFile(__parentdir + '/data/drawerbar_data.json', 'utf8', (err, data) => {
        if(err){
            console.log(err);
            return res.status(500).json({
                success: false,
                msg: 'internal server error'
            });
        }

        let json = JSON.parse(data);

        res.json({
            success: true,
            data: json
        })
    });
});


module.exports = router;

//  http://dev1mobile.smtown.com/exol
//  http://dev1mobile.smtown.com/artium