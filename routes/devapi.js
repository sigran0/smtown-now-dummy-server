
let mongoose = require('mongoose');
let passport = require('passport');
let config = require('../config/database');
let jwt = require('jsonwebtoken');

let User = require("../models/user");
let Article = require("../models/article");
let Image = require('../models/image');

let fs = require('fs');
let path = require('path');
let faker = require('faker');

let express = require('express');
let router = express.Router();

require('../config/passport')(passport);

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

router.post('/getarticle');

faker.locale = 'ko'

router.get('/getUser', (req, res) => {

    return res.json({
        name: faker.name.findName(),
        email: faker.internet.email(),
        url: faker.internet.url(),
        imageUrl: faker.image.imageUrl(),
        randomImage: faker.random.image()
    });
});

router.post('/signup', (req, res) => {

    if(!req.body.username || !req.body.password || !req.body.email){
        res.json({
            success: false,
            msg: 'Please pass username and password and email'
        });
    } else {
        let newUser = new User({
            userId: req.body.username,
            userEmail: req.body.email,
            password: req.body.password
        });

        newUser.save((err) => {
            if(err){
                return res.json({
                    success: false,
                    msg: 'Username already exists.'
                });
            }

            res.json({
                success: true,
                msg: 'Successful created new user'
            });
        });
    }
});

router.post('/signin', (req, res) => {

    User.findOne({
        userId: req.body.username
    }, (err, user) => {
        if(err)
            throw err;
        if(!user){
            res.status(401).send({
                success: false,
                msg: 'Authentication failed. Cannot find user'
            });
        } else {
            //  Check password
            user.comparePassword(req.body.password,
                (err, isMatch) => {
                if(isMatch && !err){
                    let token = jwt.sign(user, config.secret);

                    res.json({
                        success: true,
                        token: 'JWT ' + token
                    });
                } else {
                    res.status(401).send({
                        success: false,
                        msg: 'Authentication failed. Wrong password'
                    });
                }
            });
        }
    });
});

router.post('/postArticleListGetWithPage', (req, res) => {

    let page = req.body.page || 1;
    let size = req.body.size || 10;

    let start = Number((page - 1) * size);
    let end = Number(start) + Number(size);

    let type = req.body.type || 0;

    let len = end - start;

    if(len <= 0){
        res.json({
            success: false,
            msg: 'end parameter must bigger then start parameter'
        });
        return;
    }

    if(page <= 0 || size <= 0){
        res.json({
            success: false,
            msg: 'parameter page and size must bigger than 0'
        })
    }

    Article.count({
        type: type
    }, (err, size) => {

        if(err){
            res.json({
                success: false,
                msg: 'Getting Total article size Error!'
            });
            return;
        }

        Article.find({
            type: type
        })
            .sort({'date': -1})
            .skip(Number(start))
            .limit(Number(len))
            .exec((err, articles) => {

                if(!articles){
                    console.log(start);
                    res.json({
                        success: false,
                        msg: "internal error!"
                    });
                    return;
                }

                let articleSize = articles.length;
                let isLast = start + articleSize >= size ? true : false;

                res.json({
                    success: true,
                    data: {
                        size: size,
                        article_size: articleSize,
                        isLast: isLast,
                        articles: articles
                    }
                });
            });
    });
});

router.post('/postGetImageUseObjectId', (req, res) => {

    let imageId = req.body.imageid;

    Image.findOne({
        _id: imageId
    }, (err, image) => {

        if(err){
            res.json({
                success: false,
                msg: 'error parameter'
            });
            return;
        }

        res.json({
            success: true,
            image: image
        })
    });
});

router.post('/postArticleWithImage', (req, res) => {

    let title = req.body.title;
    let contents = req.body.contents;
    let linkUrl = req.body.linkUrl;
    let like = req.body.like || 0;
    let type = req.body.type || 0;
    let date = req.body.date;
    let sns_type = req.body.sns_type || 0;
    let contents_url = req.body.contents_url || null;

    let imageUrl = req.body.image.imageUrl;
    let width = req.body.image.width;
    let height = req.body.image.height;

    if(!title || !contents || !imageUrl){
        res.json.status(401)({
            success: false,
            msg: 'Plese pass title and contents and image object id'
        });
    } else {

        let article = new Article({
            title: title,
            contents: contents,
            image: {
                title: title,
                imageUrl: imageUrl,
                width: width,
                height: height
            },
            linkUrl: linkUrl,
            like: like,
            type: type,
            sns_type: sns_type,
            contents_url: contents_url,
            date: date
        });

        article.save((err) => {
            if(err){
                console.log(err);
                return res.status(401).json({
                    success: false,
                    msg: 'article save failed'
                });
            }

            res.json({
                success: true,
                msg: 'Successful write new article'
            });
        });
    }
});

router.post('/postGetDrawerBarData', (req, res) => {

    __parentdir = path.dirname(module.parent.filename);

    //  first, read drawerbar data in the data/drawerbar_data.json
    fs.readFile(__parentdir + '/data/drawerbar_data.json', 'utf8', (err, data) => {
        if(err){
            console.log(err);
            return res.status(500).json({
                success: false,
                msg: 'internal server error'
            });
        }

        let json = JSON.parse(data);

        res.json({
            success: true,
            data: json
        })
    });
});

router.post('/postGetArtistList', (req, res) => {

    __parentdir = path.dirname(module.parent.filename);

    //  first, read drawerbar data in the data/drawerbar_data.json
    fs.readFile(__parentdir + '/data/artist_list.json', 'utf8', (err, data) => {
        if(err){
            console.log(err);
            return res.status(500).json({
                success: false,
                msg: 'internal server error'
            });
        }

        let json = JSON.parse(data);

        res.json({
            success: true,
            data: json
        })
    });
});

module.exports = router;